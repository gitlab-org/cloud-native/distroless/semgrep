#pragma once 

#include <iostream>
#include <string>

class SomeClass {
    SomeClass() {};

    void Process(std::string In)
    {
        FILE *file;
        file = fopen(In.c_str(), "r");
        std::cout << "file:" << In << std::endl;
    }
};